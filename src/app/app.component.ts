import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {

  public fileInfo = { fileName: null, data: [] };

  uploadListener($event: any) {
    this.fileInfo = { fileName: null, data: [] };
    let input = $event.target;
    if(!input || !input.files || !input.files[0]) return;
    let reader = new FileReader();
    this.fileInfo.fileName = input.files[0].name;

    // utf-8
    reader.readAsText(input.files[0]);

    // CP1251
    //reader.readAsText(input.files[0], 'CP1251');

    reader.onload = () => {
      this.fileInfo.data = [];
      let csvRecords = (<string>reader.result).split(/\r\n|\n/);
      if(!csvRecords[0]) return;
      let headersRow = [];
      (<string> csvRecords[0]).split(';').forEach(f => headersRow.push(f));
      csvRecords.forEach((row, rowIndex) => {
        if (rowIndex == 0) return;
        let curruntRecord = (<string> row).split(';');
        if (curruntRecord.length != headersRow.length) return;
        let csvRecord: any = {};
        headersRow.forEach((v, indx) => { csvRecord[(<string>v).replace(/[.*+?^${}()|[\]\\]/g, '').replace(/ /g, '_')] = curruntRecord[indx] ? curruntRecord[indx].trim() : null });
        this.fileInfo.data.push(csvRecord);
      });
    };
  }
}

